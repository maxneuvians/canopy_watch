defmodule CanopyWatch.Handler do
  use JSONRPC2.Server.Handler

  def handle_request("reviews", [asin]) do
    CanopyWatch.reviews(asin)
  end
end
