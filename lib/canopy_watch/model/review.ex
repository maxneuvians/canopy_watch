defmodule CanopyWatch.Model.Review do
  defstruct id: nil,
    product_id: nil,
    title: nil,
    user_id: nil,
    user: nil,
    content: nil,
    rating: nil,
    date: nil,
    verified: nil

  @type t :: %__MODULE__{}
end
