defmodule CanopyWatch.Loader do
  @amazon_url "https://www.amazon.com/ss/customer-reviews/"

  def start_link(%{asin: asin, page: page}) do
    GenServer.start_link(__MODULE__, %{asin: asin, page: page})
  end

  def init(args) do
    send self, :fetch_content
    {:ok, args}
  end

  def handle_info(:fetch_content, %{asin: asin, page: page}) do
    result = fetch(asin, page)
    {:noreply, result}
  end

  def handle_call(:get_content, _from, state) do
    {:reply, state, state}
  end

  def fetch(asin, page \\ 1 ) do
    case HTTPotion.get(@amazon_url <> asin <> "?sortBy=recent&pageNumber=#{page}&reviewerType=all_reviews") do
      %HTTPotion.Response{status_code: 200, body: body}
        -> body
      %HTTPotion.Response{status_code: 404}
        -> :error
      {:error, _}
        -> Process.exit(self(), :kill)
    end
  end

  def terminate(_reason, _state) do
  end
end
