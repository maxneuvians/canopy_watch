defmodule CanopyWatch.Parser do

  def find_reviews(html) do
    Floki.find(html, ".a-section.review")
    |> build_reviews
  end

  def find_total_count(html) do
    {int, _} = Floki.find(html, "span.totalReviewCount")
    |> Floki.text
    |> Integer.parse
    int
  end

  def find_total_pages(html) do
    items = Floki.find(html, "ul.a-pagination li.page-button")
    if items != [] do
      {int, _} = Floki.find(html, "ul.a-pagination li.page-button")
      |> List.last
      |> Floki.text
      |> Integer.parse
      int
    else
      1
    end
  end

  defp build_reviews(reviews) do
    reviews
    |> Enum.map(fn review ->
      %CanopyWatch.Model.Review{
        id: review |> parse_id,
        product_id: review |> parse_product_id,
        title: review |> parse_title,
        user_id: review |> parse_user_id,
        user: review |> parse_user,
        content: review |> parse_content,
        rating: review |> parse_rating,
        date: review |> parse_date,
        verified: review |> parse_verfied
      }
    end)
  end

  defp parse_asin_from_url(url) do
    Regex.run(~r/(?:ASIN=)(.*$)/, url)
    |> List.last
  end

  defp parse_content(review) do
    Floki.find(review, "span.review-text")
    |> Floki.text
  end

  defp parse_date(review) do
    Floki.find(review, "span.review-date")
    |> Floki.text
    |> String.replace("on ", "")
    |> Timex.parse!("%B %e, %Y", :strftime)
  end

  defp parse_id(review) do
    Floki.attribute(review, "id") |> List.first
  end

  defp parse_product_id(review) do
    Floki.find(review, "a.review-title")
    |> Floki.attribute("href")
    |> List.first
    |> parse_asin_from_url
  end

  defp parse_rating(review) do
    {int, _} = Floki.find(review, "i.review-rating span")
    |> Floki.text
    |> Integer.parse
    int
  end

  defp parse_title(review) do
    Floki.find(review, "a.review-title")
    |> Floki.text
  end

  defp parse_user_id(review) do
    Floki.find(review, "a.author")
    |> Floki.attribute("href")
    |> List.first
    |> parse_user_id_from_url
  end

  defp parse_user_id_from_url(url) do
    Regex.run(~r/(?:profile)\/(.*)\//, url)
    |> List.last
  end

  defp parse_user(review) do
    Floki.find(review, "a.author")
    |> Floki.text
  end

  defp parse_verfied(review) do
    case Floki.find(review, ".a-size-mini.a-color-state.a-text-bold") do
      [] -> false
      _ -> true
    end
  end

end
