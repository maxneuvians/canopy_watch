defmodule CanopyWatch do
  @moduledoc """
  Contains functions to parse reviews from Amazon for a product specified by an
  ASIN.
  """
  use Application
  alias CanopyWatch.{Handler, Loader, Parser}
  require Logger

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    JSONRPC2.Servers.TCP.start_listener(Handler, 8123)

    children = [
      worker(CanopyWatch.Loader, [], restart: :permanent)
    ]
    opts = [strategy: :simple_one_for_one, name: CanopyWatch.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def reviews(asin \\ "B0142PHFVU") when is_binary(asin) do
    Loader.fetch(asin)
    |> get_reviews(asin)
  end

  def reviews(asin, :verify) when is_binary(asin) do
    html = Loader.fetch(asin)
    get_reviews(html, asin)
    |> verify_results(html)
  end

  defp get_reviews(html, asin) do
    Enum.map(1..Parser.find_total_pages(html), fn page ->
      Logger.debug "Calling page #{page}"
      {:ok, pid} = Supervisor.start_child(CanopyWatch.Supervisor, [%{asin: asin, page: page}])
      pid
    end)
    |> Enum.map(fn pid ->
      GenServer.call(pid, :get_content, 10_000)
      |> Parser.find_reviews
    end)
    |> Enum.reduce([], fn(r, acc) -> acc ++ r end)
  end

  defp verify_results(results, html) do
    if Parser.find_total_count(html) == length(results) do
      {:ok, results}
    else
      {:error, :count_mismatch}
    end
  end
end
