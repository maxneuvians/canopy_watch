# CanopyWatch

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add canopy_watch to your list of dependencies in `mix.exs`:

        def deps do
          [{:canopy_watch, "~> 0.0.1"}]
        end

  2. Ensure canopy_watch is started before your application:

        def application do
          [applications: [:canopy_watch]]
        end

