defmodule CanopyWatch.Mixfile do
  use Mix.Project

  def project do
    [app: :canopy_watch,
     version: "0.0.1",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps]
  end

  def application do
    [mod: {CanopyWatch, []},
    applications: [:floki, :timex, :hackney, :uuid, :httpotion, :jsonrpc2, :logger, :poison, :ranch]]
  end

  defp deps do
    [
      {:distillery, "~> 0.10"},
      {:floki, "~> 0.8"},
      {:httpotion, "~> 3.0.2"},
      {:jsonrpc2, "~> 0.4"},
      {:poison, "~> 2.2"},
      {:ranch, "~> 1.2"},
      {:timex, "~> 2.1"},
      {:uuid, "~> 1.1"}
    ]
  end
end
